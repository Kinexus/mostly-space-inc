<?php
/**
 * Shipyard
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Ships
 */

class MsiShips extends MsiCore {

  public function __construct() {
    add_action('init', [$this, 'msinc_create_ships'], 0);

    // Create required custom taxonomies and terms
    add_action('init', [$this, 'msinc_create_ship_type_taxonomy'], 0);
    add_action('init', [$this, 'msinc_install_ship_terms']);
  }

  // Register Ship Custom Post Type
  public function msinc_create_ships() {
    $cpt_labels = [
      'name'                  => _x( 'Ships', 'Post Type General Name', 'msinc_cpt' ),
      'singular_name'         => _x( 'Ship', 'Post Type Singular Name', 'msinc_cpt' ),
      'menu_name'             => __( 'Ships', 'msinc_cpt' ),
      'name_admin_bar'        => __( 'Ships', 'msinc_cpt' ),
      'archives'              => __( 'Ship Archives', 'msinc_cpt' ),
      'attributes'            => __( 'Ship Attributes', 'msinc_cpt' ),
      'parent_item_colon'     => __( 'Parent Ship:', 'msinc_cpt' ),
      'all_items'             => __( 'All Ships', 'msinc_cpt' ),
      'add_new_item'          => __( 'Add New Ship', 'msinc_cpt' ),
      'add_new'               => __( 'Add New Ship', 'msinc_cpt' ),
      'new_item'              => __( 'New Ship', 'msinc_cpt' ),
      'edit_item'             => __( 'Edit Ship', 'msinc_cpt' ),
      'update_item'           => __( 'Update Ship', 'msinc_cpt' ),
      'view_item'             => __( 'View Ship', 'msinc_cpt' ),
      'view_items'            => __( 'View Ships', 'msinc_cpt' ),
      'search_items'          => __( 'Search Ships', 'msinc_cpt' ),
      'not_found'             => __( 'Ship not found', 'msinc_cpt' ),
      'not_found_in_trash'    => __( 'Ship not found in Trash', 'msinc_cpt' ),
      'featured_image'        => __( 'Featured Image', 'msinc_cpt' ),
      'set_featured_image'    => __( 'Set featured image', 'msinc_cpt' ),
      'remove_featured_image' => __( 'Remove featured image', 'msinc_cpt' ),
      'use_featured_image'    => __( 'Use as featured image', 'msinc_cpt' ),
      'insert_into_item'      => __( 'Insert into game page', 'msinc_cpt' ),
      'uploaded_to_this_item' => __( 'Uploaded to Ships', 'msinc_cpt' ),
      'items_list'            => __( 'Entire Ship list', 'msinc_cpt' ),
      'items_list_navigation' => __( 'Entire Ship list navigation', 'msinc_cpt' ),
      'filter_items_list'     => __( 'Filter Ship list', 'msinc_cpt' ),
    ];
    $cpt_args = [
      'label'                 => __( 'Ship', 'msinc_cpt' ),
      'description'           => __( 'Meant for bridging gaps between space...', 'msinc_cpt' ),
      'labels'                => $cpt_labels,
      'supports'              => ['title', 'editor', 'custom-fields', 'page-attributes', 'post-formats'],
      'taxonomies'            => ['ship-types'],
      'hierarchical'          => true,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 100,
      'menu_icon'							=> 'dashicons-performance',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => true,
      'publicly_queryable'    => true,
      'show_in_rest'					=> true,
      'capability_type'       => 'page'
    ];
    register_post_type( 'ships', $cpt_args );
  }

  // Register Ship Type Taxonomy
  public function msinc_create_ship_type_taxonomy() {

    $labels = [
      'name'                       => _x( 'Ship Types', 'Taxonomy General Name', 'msinc_cpt' ),
      'singular_name'              => _x( 'Ship Type', 'Taxonomy Singular Name', 'msinc_cpt' ),
      'menu_name'                  => __( 'Ship Types', 'msinc_cpt' ),
      'all_items'                  => __( 'All Ship Types', 'msinc_cpt' ),
      'parent_item'                => __( 'Parent Ship Type', 'msinc_cpt' ),
      'parent_item_colon'          => __( 'Parent Ship Type:', 'msinc_cpt' ),
      'new_item_name'              => __( 'New Ship Type Name', 'msinc_cpt' ),
      'add_new_item'               => __( 'Add Ship Type', 'msinc_cpt' ),
      'edit_item'                  => __( 'Edit Ship Type', 'msinc_cpt' ),
      'update_item'                => __( 'Update Ship Type', 'msinc_cpt' ),
      'view_item'                  => __( 'View Ship Type', 'msinc_cpt' ),
      'separate_items_with_commas' => __( 'Separate Ship Types with commas', 'msinc_cpt' ),
      'add_or_remove_items'        => __( 'Add or remove Ship Types', 'msinc_cpt' ),
      'choose_from_most_used'      => __( 'Choose from the most used', 'msinc_cpt' ),
      'popular_items'              => __( 'Popular Ship Types', 'msinc_cpt' ),
      'search_items'               => __( 'Search Ship Types', 'msinc_cpt' ),
      'not_found'                  => __( 'Ship Type Not Found', 'msinc_cpt' ),
      'no_terms'                   => __( 'No Ship Types', 'msinc_cpt' ),
      'items_list'                 => __( 'Ship Type list', 'msinc_cpt' ),
      'items_list_navigation'      => __( 'Ship Type list navigation', 'msinc_cpt' ),
    ];
    $args = [
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => false
    ];
    register_taxonomy( 'ship-types', ['ships'], $args );
  }

  // Register Ship Terms
  public function msinc_install_ship_terms() {
    $target_taxonomies =[
      ['general-purpose', 'General Purpose', '', 'General purpose ship'],

      ['explorer', 'Explorer', '', 'Specialized ship for exploration'],
        ['surveyor', 'Surveyor', 'explorer', 'Requires scientist'],     // Surveying Specialized
        ['surface', 'Surface', 'explorer', 'Requires scientist'],       // Surface Exploration Specialized
        ['long-range', 'Long-Range', 'explorer', 'Requires engineer'],  // Distance Specialized

      ['mining', 'Mining', '', 'Specialized ship for mining'],
        ['driller', 'Driller', 'mining', 'Requires miner'],           // Drilling Specialized
        ['charger', 'Charger', 'mining', 'Requires miner'],           // Explosive Specialized
        ['collecter', 'Collecter', 'mining', 'Requires miner'],       // Collection Specialized

      ['utility', 'Utility', '', 'Ships specialized for utility and repair'],
        ['repair', 'Repair', 'utility', 'Requires mechanic'],                   // Requires mechanic
        ['colonization', 'Colonization', 'utility', 'Requires engineer, scientist, and diplomat'],

      ['transport', 'Transport', '', 'Ships specialized for getting people and things to other places'],
        ['passenger-transport', 'Passenger Transport', 'transport', 'Requires flight attendant'],         // Passenger Specialized
        ['freight-transport', 'Freight Transport', 'transport', ''],                                      // Freight Specialized
        ['fuel-transport', 'Fuel Transport', 'transport', ''],                                            // Fuel Specialized

      ['combat', 'Combat', '', 'Ships specialized for battle'],
        ['gunner', 'Gunner', 'combat', 'Requires gunner pilot'],    // Shooter
        ['trapper', 'Trapper', 'combat', 'Requires gunner pilot'],  // Mine Dropper
        ['turret', 'Turret', 'combat', ''],                         // Automated Turrets
        ['tank', 'Tank', 'combat', '']                              // Strong shields/hull
    ];
    foreach($target_taxonomies as $target_taxonomy) {
      if(term_exists($target_taxonomy[0], 'ship-types')) {
        //self::msinc_error('Term already exists:' . $target_taxonomy[0]);
        continue;
      }

      $parent_taxonomy = term_exists($target_taxonomy[2], 'ship-types');
      if(!empty($parent_taxonomy['term_id'])) {
        $args = [
          'parent'			=> $parent_taxonomy['term_id'],
          'description' => $target_taxonomy[3],
          'slug'				=> $target_taxonomy[0]
        ];
      } else {
        $args = [
          'description' => $target_taxonomy[3],
          'slug'				=> $target_taxonomy[0]
        ];
      }
      wp_insert_term($target_taxonomy[1], 'ship-types', $args);
      //self::msinc_error(var_dump($parent_taxonomy));
    }
  }
}
