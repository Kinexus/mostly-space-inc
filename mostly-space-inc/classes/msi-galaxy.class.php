<?php
/**
 * Galaxy and celestial body functions
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Galaxy
 */

class MsiGalaxy extends MsiCore {

  public function __construct() {
    // Big Bang
    add_action('init', [$this, 'msinc_create_galaxy'], 0);

    // Let there be objects
    add_action('init', [$this, 'msinc_create_galaxy_object_taxonomy'], 0);
    add_action('init', [$this, 'msinc_install_galaxy_object_terms']);

	// Let there be structure
	add_filter('single_template', [$this, 'msinc_system_template']);

	// Let there be style
	add_action('wp_enqueue_scripts', [$this, 'enqueue_solar_system_scripts']);
  }

  /*
   *	Creating the galaxy!
   */
  public function msinc_create_galaxy() {
    // Register Custom Post Type
    $cpt_labels = [
      'name'                  => _x( 'Galaxy', 'Post Type General Name', 'msinc_cpt' ),
      'singular_name'         => _x( 'Galaxy', 'Post Type Singular Name', 'msinc_cpt' ),
      'menu_name'             => __( 'Galaxy', 'msinc_cpt' ),
      'name_admin_bar'        => __( 'Galaxy', 'msinc_cpt' ),
      'archives'              => __( 'Galaxy Archives', 'msinc_cpt' ),
      'attributes'            => __( 'Galaxy Attributes', 'msinc_cpt' ),
      'parent_item_colon'     => __( 'Parent Object:', 'msinc_cpt' ),
      'all_items'             => __( 'Entire Galaxy', 'msinc_cpt' ),
      'add_new_item'          => __( 'Add New Object', 'msinc_cpt' ),
      'add_new'               => __( 'Add New Object', 'msinc_cpt' ),
      'new_item'              => __( 'New Oject', 'msinc_cpt' ),
      'edit_item'             => __( 'Edit Object', 'msinc_cpt' ),
      'update_item'           => __( 'Update Object', 'msinc_cpt' ),
      'view_item'             => __( 'View Object', 'msinc_cpt' ),
      'view_items'            => __( 'View Objects', 'msinc_cpt' ),
      'search_items'          => __( 'Search Objects', 'msinc_cpt' ),
      'not_found'             => __( 'Not found', 'msinc_cpt' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'msinc_cpt' ),
      'featured_image'        => __( 'Featured Image', 'msinc_cpt' ),
      'set_featured_image'    => __( 'Set featured image', 'msinc_cpt' ),
      'remove_featured_image' => __( 'Remove featured image', 'msinc_cpt' ),
      'use_featured_image'    => __( 'Use as featured image', 'msinc_cpt' ),
      'insert_into_item'      => __( 'Insert into game page', 'msinc_cpt' ),
      'uploaded_to_this_item' => __( 'Uploaded to game page', 'msinc_cpt' ),
      'items_list'            => __( 'Entire Galaxy list', 'msinc_cpt' ),
      'items_list_navigation' => __( 'Entire Galaxy list navigation', 'msinc_cpt' ),
      'filter_items_list'     => __( 'Filter Galaxy list', 'msinc_cpt' ),
    ];
		$cpt_rewrite = [
			'slug'                  => 'galaxy',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => false,
		];
    $cpt_args = [
      'label'                 => __( 'Galaxy', 'msinc_cpt' ),
      'description'           => __( 'Mostly space...', 'msinc_cpt' ),
      'labels'                => $cpt_labels,
      'supports'              => ['title', 'editor', 'custom-fields', 'page-attributes', 'post-formats'],
      'taxonomies'            => ['galaxy-objects'],
      'hierarchical'          => true,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 100,
      'menu_icon'							=> 'dashicons-rss',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => true,
      'publicly_queryable'    => true,
			'query_var'							=> true,
			'rewrite'            		=> $cpt_rewrite,
      'show_in_rest'					=> true,
      'capability_type'       => 'page'
    ];
    register_post_type('galaxy', $cpt_args);
  }

  // Register Galaxy Object Taxonomy
  public function msinc_create_galaxy_object_taxonomy() {

    $tax_labels = [
      'name'                       => _x( 'Galaxy Objects', 'Taxonomy General Name', 'msinc_cpt' ),
      'singular_name'              => _x( 'Galaxy Object', 'Taxonomy Singular Name', 'msinc_cpt' ),
      'menu_name'                  => __( 'Galaxy Objects', 'msinc_cpt' ),
      'all_items'                  => __( 'All Galaxy Objects', 'msinc_cpt' ),
      'parent_item'                => __( 'Parent Object', 'msinc_cpt' ),
      'parent_item_colon'          => __( 'Parent Object:', 'msinc_cpt' ),
      'new_item_name'              => __( 'New Galaxy Object Name', 'msinc_cpt' ),
      'add_new_item'               => __( 'Add New Galaxy Object', 'msinc_cpt' ),
      'edit_item'                  => __( 'Edit Galaxy Object', 'msinc_cpt' ),
      'update_item'                => __( 'Update Galaxy Object', 'msinc_cpt' ),
      'view_item'                  => __( 'View Galaxy Object', 'msinc_cpt' ),
      'separate_items_with_commas' => __( 'Separate objects with commas', 'msinc_cpt' ),
      'add_or_remove_items'        => __( 'Add or remove Galaxy Objects', 'msinc_cpt' ),
      'choose_from_most_used'      => __( 'Choose from the most used', 'msinc_cpt' ),
      'popular_items'              => __( 'Popular Galaxy Objects', 'msinc_cpt' ),
      'search_items'               => __( 'Search Galaxy Objects', 'msinc_cpt' ),
      'not_found'                  => __( 'Not Found', 'msinc_cpt' ),
      'no_terms'                   => __( 'No Galaxy Objects', 'msinc_cpt' ),
      'items_list'                 => __( 'Galaxy Object list', 'msinc_cpt' ),
      'items_list_navigation'      => __( 'Galaxy Object list navigation', 'msinc_cpt' ),
    ];
		$tax_rewrite = [
			'slug'                  => 'galaxy',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => false,
		];
    $tax_args = [
      'labels'                    => $tax_labels,
			'rewrite'										=> $tax_rewrite,
      'hierarchical'              => true,
      'public'                    => true,
      'show_ui'                   => true,
      'show_admin_column'					=> true,
      'show_in_nav_menus'					=> true,
			'show_in_rest'							=> true,
			'query_var'									=> true,
      'show_tagcloud'             => false
    ];
    register_taxonomy('galaxy-objects', ['galaxy'], $tax_args);
  }

  // Register Galaxy Object Terms
  public function msinc_install_galaxy_object_terms() {
    $target_taxonomies =[
      ['system', 'System', ''],
      // Stars
      ['star', 'Star', 'system'],
        // Star sizes
				['neutraon-star', 'Neutron Star', 'star'],
				['dwarf-star', 'Dwarf Star', 'star'],
				['compact-star', 'Compact Star', 'star'],
        ['giant-star', 'Giant Star', 'star'],
        ['super-giant-star', 'Super Giant Star', 'star'],

        // Star temperatures
        ['o-blue-star', 'O (blue)', 'star'],
        ['b-blue-white-star', 'B (Blue-White)', 'star'],
        ['a-white-star', 'A (White)', 'star'],
        ['f-yellow-white', 'F (Yellow-White)', 'star'],
        ['g-yellow', 'G (Yellow)', 'star'],
        ['k-orange', 'K (Orange)', 'star'],
        ['m-red', 'M (Red)', 'star'],

      // Celestial Bodies
      ['planet', 'Planet', 'system'],
        // Planet size
        ['giant-planet', 'Giant Planet', 'planet'],
        ['dwarf-planet', 'Dwarf Planet', 'planet'],
        // Planet characteristics
        ['habitable-planet', 'Habitable Planet', 'planet'],
        ['core-less', 'Core-less', 'planet'],
        ['ringed-planet', 'Ringed Planet', 'planet'],
        ['gas-planet', 'Gas Planet', 'planet'],
        ['ice-planet', 'Ice Planet', 'planet'],
        ['ocean-planet', 'Ocean Planet', 'planet'],
          // Types of liquid/ice planets
          ['hydrogen-planet', 'Hydrogen Planet', 'planet'],
          ['ammonia-planet', 'Amonia Planet', 'planet'],
          ['co2-planet', 'Carbon Dioxide Planet', 'planet'],

      ['moon', 'Moon', 'system'],

      ['asteroid-belt', 'Asteroid Belt', 'system'],
      ['asteroid', 'Asteroid', 'asteroid-belt'],
        // Types of asteroids
        ['dark-c-asteroid', 'Dark C Asteroid', 'asteroid'],				// Mostly Carbon
        ['bright-s-asteroid', 'Bright S Asteroid', 'asteroid'],		// Mostly Silicate
        ['bright-m-asteroid', 'Bright M Asteroid', 'asteroid'],		// Mostly Metalic
        // Materials on asteroids
        ['hydrogen-asteroid', 'Hydrogen Asteroid', 'asteroid'],		// Used for fuel & water
        ['carbon-asteroid', 'Carbon Asteroid', 'asteroid'],				// Used in alloys to upgrade items
        ['nitrogen-asteroid', 'Nitrogen Asteroid', 'asteroid'],		// Used for breathable air
        ['oxygen-asteroid', 'Oxygen Asteroid', 'asteroid'],				// Used for breathable air and water
				['nickel-asteroid', 'Nickel Asteroid', 'asteroid'],				// Tier 1 building material
        ['cobalt-asteroid', 'Cobalt Asteroid', 'asteroid'],				// Tier 2 building material
				['iron-asteroid', 'Iron Asteroid', 'asteroid'],						// Tier 3 building material
				['osmium-asteroid', 'Osmium Asteroid', 'asteroid'],				// Tier 4 building material
        ['ruthenium-asteroid', 'Ruthenium Asteroid', 'asteroid'],	// Used for electronics, especially solar panels
        ['rhodium-asteroid', 'Rhodium Asteroid', 'asteroid'],			// Used for electronics
        ['palladium-asteroid', 'Palladium Asteroid', 'asteroid'],	// Used for electronics
        ['iridium-asteroid', 'Iridium Asteroid', 'asteroid'],			// Used for electronics
        ['platinum-asteroid', 'Platinum Asteroid', 'asteroid'],		// Used for electronics
        ['silicate-asteroid', 'Silicate Asteroid', 'asteroid']		// Used for electronics
    ];
    foreach($target_taxonomies as $target_taxonomy) {
      if(term_exists($target_taxonomy[0], 'galaxy-objects')) {
        //self::msinc_error('Term already exists:' . $target_taxonomy[0]);
        continue;
      }

      $parent_taxonomy = term_exists($target_taxonomy[2], 'galaxy-objects');
      if(!empty($parent_taxonomy['term_id'])) {
        $args = [
          'parent'			=> $parent_taxonomy['term_id'],
          'slug'				=> $target_taxonomy[0]
        ];
      } else {
        $args = [
          'slug'				=> $target_taxonomy[0]
        ];
      }
      wp_insert_term($target_taxonomy[1], 'galaxy-objects', $args);
      //self::msinc_error(var_dump($parent_taxonomy));
    }
  }

	// Register Custom Galaxy Single Page Template
	public function msinc_system_template() {
		MsiCore::msinc_notice('Registering System Template...');
		return plugin_dir_path( __FILE__ ) . '../templates/msi-system.template.php';
	}

	public function enqueue_solar_system_scripts() {
		wp_register_style('msi-system-style', '/wp-content/plugins/mostly-space-inc/templates/css/msi-system.style.css');
		wp_enqueue_style('msi-system-style', '/wp-content/plugins/mostly-space-inc/templates/css/msi-system.style.css');

		wp_register_script('msi-system-script', '/wp-content/plugins/mostly-space-inc/templates/js/msi-system.script.js');
		wp_enqueue_script('msi-system-script', '/wp-content/plugins/mostly-space-inc/templates/js/msi-system.script.js', ['jquery'], false, true);
	}
}
