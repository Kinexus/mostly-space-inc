<?php
/**
 * Game Settings functions.
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Classes/Settings
 */

class MsiSettings {
	public $msi_opt_name = 'MSI';	// Universal options name

	protected $msi_section = [];
	protected $msi_setting = [];

	/* Construct */
	public function __construct() {
		if(!class_exists('Redux')) { return; }
		// Only call settings page on game install.

		self::init_settings();
		self::enable_modules();
	}

	private function init_settings() {
		global $msi_version;

		// Set option arguments
		$msi_redux_args = [
			'opt_name'						=> $this->msi_opt_name,
			'global_variable'				=> '',
			'display_name'					=> 'Mostly Space Inc.',
			'display_version'				=> $msi_version,
			'page_permissions'				=> 'manage_options',
			'menu_type'						=> 'menu',
			'allow_sub_menu'				=> true,
			'menu_title'					=> 'Mostly Space Inc.',
			'page_title'					=> 'Mostly Space Inc.',
			'google_api_key'				=> '',
			'google_update_weekly'			=> true,
			'async_typography'				=> true,
			'admin_bar'						=> true,
			'admin_bar_icon'				=> 'dashicons-admin-site-alt3',
			'admin_bar_priority'			=> 99,
			'global_bariable'				=> 'Mostly Space Inc.',
			'dev_mode'						=> true,
			'update_notice'					=> false,
			'customizer'					=> false,
			'page_priority'					=> null,
			'ajax_save'						=> false
		];
		Redux::setArgs($this->msi_opt_name, $msi_redux_args);

		// General Settings Section
		$this->msi_section['general_section'] = [
			'id'	=> 'msi-general',
			'title'	=> 'General',
			'icon'	=> 'el el-cogs'
		];

		// Debug Subsection
		$this->msi_section['msi_debug_section'] = [
			'id'			=> 'msi-debug',
			'title'			=> 'Debug',
			'heading'		=> 'Debug',
			'desc'			=> 'Helpful functions for the event that things aren\'t looking right.',
			'subsection'	=> true,
			'fields'		=> [
				[ // Retain Settings
					'id'		=> 'msi-save-settings',
					'type'		=> 'switch',
					'title'		=> 'Keep Settings on uninstall?',
					'desc'		=> 'Useful if you plan to reinstall in the future.',
					'on'		=> 'Yes',
					'off'		=> 'No',
					'default'	=> 1
				],
				[ // Debug Mode
					'id'		=> 'msi-debug-mode',
					'type'		=> 'switch',
					'title'		=> 'Enable Debug Mode',
					'desc'		=> 'Debug messages will show up in console.',
					'on'		=> 'Yes',
					'off'		=> 'No',
					'default'	=> 0
				],
				[ // Reinitialize Sol
					'id'		=> 'msi-restore-sol',
					'type'		=> 'checkbox',
					'title'		=> 'Restore Sol',
					'desc'		=> 'Check box to programatically fix Sol. Function will repeat until unchecked.',
					'default'	=> 0
				]
			]
		];

		// Modules Subsection
		$this->msi_section['msi_modules_section'] = [
			'id'			=> 'msi-modules',
			'title'			=> 'Modules',
			'heading'		=> 'Modules',
			'desc'			=> 'Enable/Disable modules here.',
			'subsection'	=> true,
			'fields'		=> [
				[ // Analytics Module
					'id'		=> 'msi-analytics',
					'type'		=> 'switch',
					'title'		=> 'Analytics',
					'desc'		=> 'Simple tracking code input for analytics failsafe.',
					'on'		=> 'Enabled',
					'off'		=> 'Disabled',
					'default'	=> 0
				]
			]
		];

		// Create sections
		self::create_sections($this->msi_section);

		// Create settings
		self::create_settings($this->msi_setting);
	}

	private function create_sections($sections) {
		if(empty($sections)) { return; }
		foreach($sections as $section) {
			Redux::setSection($this->msi_opt_name, $section);
		}
	}

	private function create_settings($settings) {
		if(empty($sections)) { return; }
		foreach($settings as $setting) {
			Redux::setField($this->msi_opt_name, $setting);
		}
	}

	private function enable_modules() {
		// Get Options
		global $msi_opt_name;
		$options = get_option($this->msi_opt_name);

		if($options['msi-analytics'] == '1') {
			require_once plugin_dir_path(__FILE__) . 'msi-analytics.class.php';
			$msi_analytics = new MsiAnalytics;
		}
		if($options['msi-restore-sol'] == '1') {
			add_action('wp_loaded', [$this, 'msinc_create_sol']);
		}
	}

	/*
	 * Debug Options
	 */
	public function msinc_error($str) {
		$error_str = strval($str);
		global $msi_opt_name;
		$options = get_option($this->msi_opt_name);

		if($options['msi-debug-mode'] == 1) {
	 		?>
				<script>console.log('MSI Error: <?php echo $error_str; ?>');</script>
	 		<?php
		}
	}

	public function msinc_notice($str) {
		$notice_str = strval($str);
		global $msi_opt_name;
		$options = get_option($this->msi_opt_name);

		if($options['msi-debug-mode'] == 1) {
			?>
	 			<script>console.log('MSI notice: <?php echo $notice_str; ?>');</script>
	 		<?php
		}
	}

	public function msinc_create_sol() {
		self::msinc_notice('Create Sol Debug Option Enabled.');
		$user_id = get_current_user_id();
		if(is_user_logged_in() && user_can($user_id, 'update_core') && is_admin()) {

			// Sol System
			if(!get_page_by_title('Sol', OBJECT, 'galaxy')){
				$post_data = ['post_title' => 'Sol', 'menu_order' => 0, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'system', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Sol already exists.');

			// Sun
			if(!get_page_by_title('Sun', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sol', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Sun', 'menu_order' => 0, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, ['star', 'g-yellow'], 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Sun already exists.');

			// Mercury
			if(!get_page_by_title('Mercury', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Mercury', 'menu_order' => 0, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Mercury already exists.');

			// Venus
			if(!get_page_by_title('Venus', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Venus', 'menu_order' => 1, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Venus already exists.');

			// Earth
			if(!get_page_by_title('Earth', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Earth', 'menu_order' => 2, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Earth already exists.');

			// Moon
			if(!get_page_by_title('Moon', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Earth', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Moon', 'menu_order' => 0, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'moon', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Moon already exists.');

			// Mars
			if(!get_page_by_title('Mars', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Mars', 'menu_order' => 3, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Mars already exists.');

			// Phobos
			if(!get_page_by_title('Phobos', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Mars', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Phobos', 'menu_order' => 0, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'moon', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Phobos already exists.');

			// Phobos
			if(!get_page_by_title('Deimos', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Mars', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Deimos', 'menu_order' => 1, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'moon', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Deimos already exists.');

			// Asteroid Belt
			if(!get_page_by_title('Asteroid Belt', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Asteroid Belt', 'menu_order' => 4, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'asteroid-belt', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Asteroid Belt already exists.');

			// Ceres
			if(!get_page_by_title('Ceres', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Ceres', 'menu_order' => 0, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'dwarf-planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Ceres already exists.');

			// Vesta
			if(!get_page_by_title('Vesta', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Vesta', 'menu_order' => 1, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'asteroid', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Vesta already exists.');

			// Pallas
			if(!get_page_by_title('Pallas', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Pallas', 'menu_order' => 2, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'asteroid', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Pallas already exists.');

			// Hygiea
			if(!get_page_by_title('Hygiea', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Hygiea', 'menu_order' => 3, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'asteroid', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Hygiea already exists.');

			// Jupiter
			if(!get_page_by_title('Jupiter', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Jupiter', 'menu_order' => 5, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Jupiter already exists.');

			// Saturn
			if(!get_page_by_title('Saturn', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Saturn', 'menu_order' => 6, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Saturn already exists.');

			// Uranus
			if(!get_page_by_title('Uranus', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Uranus', 'menu_order' => 7, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Uranus already exists.');

			// Neptune
			if(!get_page_by_title('Neptune', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Neptune', 'menu_order' => 8, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Neptune already exists.');

			// Pluto
			if(!get_page_by_title('Pluto', OBJECT, 'galaxy')){
				$parent = get_page_by_title('Sun', OBJECT, 'galaxy');
				$post_data = ['post_title' => 'Pluto', 'menu_order' => 9, 'post_parent' => $parent->ID, 'post_status' => 'publish', 'author' => $user_id, 'post_type' => 'galaxy'];
				$post_id = wp_insert_post($post_data);
				wp_set_object_terms($post_id, 'dwarf-planet', 'galaxy-objects');
				self::msinc_notice($post_data['post_title'] . ' created.');
			}
			self::msinc_notice('Pluto already exists.');
		}
	}
}
