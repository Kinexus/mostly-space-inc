<?php
/**
 * Fired on plugin activation. Not particularly effective on network installs, so activation items are done via checks within the core class.
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/activate
 */

class MsiActivate extends MsiSettings {

	public function __construct() {

		// Create required game pages if they don't already exist
		self::msinc_install_pages();
	}

	private function msinc_install_pages() {
		$target_slugs = [
			'Ship',
			'Solar System',
			'Celestial Body',
			'Colony'
		];
		foreach($target_slugs as $target_slug) {
			$args = [
				'name'           => $target_slug,
				'post_type'      => 'game-page',
				'post_status'    => 'publish',
				'posts_per_page' => 1
			];
			$target_page = get_posts($args);
			if($target_page) {
				continue;
			} else {
				wp_insert_post(['post_title'=>$target_slug, 'post_status'=>'publish', 'post_type'=>'game-page'], true);
			}
		}
	}
}
