<?php
/**
 * Initializes page templates for game pages.
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Page Templates
 */

MsiTemplates extends MsiCore {

  public function __construct() {

  }
}
