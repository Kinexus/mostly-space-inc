<?php
/**
 * Core functions
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Core
 */

class MsiCore extends MsiSettings {

	public function __construct() {
		// Enqueue FontAwesome - Admin Icons in admin css.
		add_action('admin_init', [$this, 'enqueue_fontawesome_dashboard']);

		// Create required custom post types
		add_action('init', [$this, 'msinc_create_game_pages'], 0);
	}

	// Allow Font Awesome 5 Icons as dashicons
	public function enqueue_fontawesome_dashboard() {
		wp_enqueue_style('fontawesome', '/wp-content/plugins/mostly-space-inc/includes/font-awesome/css/all.min.css', '', '5.12.0', 'all');
	}

	/*
	 * Create Game Pages
	 */
	public function msinc_create_game_pages() {
		// Register Custom Post Type
		$cpt_labels = [
			'name'                  => _x('Game Pages', 'Post Type General Name', 'msinc_cpt'),
			'singular_name'         => _x('Game Page', 'Post Type Singular Name', 'msinc_cpt'),
			'menu_name'             => __('Game Pages', 'msinc_cpt'),
			'name_admin_bar'        => __('Game Page', 'msinc_cpt'),
			'archives'              => __('Game Page Archives', 'msinc_cpt'),
			'attributes'            => __('Game Page Attributes', 'msinc_cpt'),
			'parent_item_colon'     => __('Parent Game Page:', 'msinc_cpt'),
			'all_items'             => __('All Game Pages', 'msinc_cpt'),
			'add_new_item'          => __('Add New Game Page', 'msinc_cpt'),
			'add_new'               => __('Add New', 'msinc_cpt'),
			'new_item'              => __('New Game Page', 'msinc_cpt'),
			'edit_item'             => __('Edit Game Page', 'msinc_cpt'),
			'update_item'           => __('Update Game Page', 'msinc_cpt'),
			'view_item'             => __('View Game Page', 'msinc_cpt'),
			'view_items'            => __('View Game Pages', 'msinc_cpt'),
			'search_items'          => __('Search Game Pages', 'msinc_cpt'),
			'not_found'             => __('Not found', 'msinc_cpt'),
			'not_found_in_trash'    => __('Not found in Trash', 'msinc_cpt'),
			'featured_image'        => __('Featured Image', 'msinc_cpt'),
			'set_featured_image'    => __('Set featured image', 'msinc_cpt'),
			'remove_featured_image' => __('Remove featured image', 'msinc_cpt'),
			'use_featured_image'    => __('Use as featured image', 'msinc_cpt'),
			'insert_into_item'      => __('Insert into game page', 'msinc_cpt'),
			'uploaded_to_this_item' => __('Uploaded to game page', 'msinc_cpt'),
			'items_list'            => __('Game page list', 'msinc_cpt'),
			'items_list_navigation' => __('Game page list navigation', 'msinc_cpt'),
			'filter_items_list'     => __('Filter game page list', 'msinc_cpt')
		];
		$cpt_rewrite = [
			'slug'                  => 'game-page',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => false
		];
		$cpt_args = [
			'label'                 => __('Game Page', 'msinc_cpt'),
			'description'           => __('Game Page Description', 'msinc_cpt'),
			'labels'                => $cpt_labels,
			'supports'              => ['title', 'editor', 'custom-fields', 'page-attributes', 'post-formats'],
			'taxonomies'            => ['category'],
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 100,
			'menu_icon'				=> 'dashicons-admin-site-alt3',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'query_var'				=> true,
			'rewrite'            	=> $cpt_rewrite,
			'show_in_rest'			=> true,
			'capability_type'       => 'page'
		];
		register_post_type('game-page', $cpt_args);
	}
}
