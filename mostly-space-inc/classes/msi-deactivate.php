<?php
/**
 * Fired on plugin deactivation
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/deactivate
 */

class MsiDeactivate extends MsiSettings {

	public function __construct() {
		self::msinc_deactivate();
	}

	public function msinc_deactivate() {

	}
}
