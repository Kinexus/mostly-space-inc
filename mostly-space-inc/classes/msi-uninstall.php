<?php
/**
 * Fired on plugin uninstall
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc.
 * @subpackage Mostly Space Inc./includes
 */

class MsiUninstall extends MsiSettings {

	public function __construct() {
		self::msinc_uninstall();
	}

	public function msinc_uninstall() {
		if(!class_exists('Redux')) { return; }
		// Get Options
		global $msi_opt_name;
		$options = get_option($this->msi_opt_name);

		//delete_option("Searle");
		if($options['msi-save-settings'] == '0'){
			delete_option($this->msi_opt_name);
		}

		// Deregister Custom Post types
		unregister_post_type('game_pages');
	}
}
