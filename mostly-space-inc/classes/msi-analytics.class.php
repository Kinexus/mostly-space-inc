<?php
/**
 * Fired when analytics options are enabled
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/analytics
 */

class MsiAnalytics extends MsiSettings {

	public function __construct() {
		if(!class_exists('Redux')) { return; }
		self::init_settings();
		self::msinc_hook_analytics();
	}

	public function init_settings() {
		if(is_admin()) {

			// Analytics Settings Section
			$this->msi_section['analytics_section'] = [
				'id'			=> 'msi-analytics',
				'title'		=> 'Analytics',
				'icon'		=> 'el el-graph'
			];

			$this->msi_section['msi_analytics_section'] = [
				'id'					=> 'msi-analytics-tracking-code',
				'title'				=> 'Tracking Code',
				'heading'			=> 'Tracking Code',
				'desc'				=> 'Find your tracking code in Google Analytics > Admin > Property > Tracking Info.',
				'subsection'	=> true,
				'fields'			=> [
					[
						'id'					=> 'msi-analytics-code',
						'type'				=> 'text',
						'title'				=> 'Analytics Tracking Code',
						'desc'				=> 'Tracking code should look like "UA-XXXXX-X".',
						'default'			=> 'UA-XXXXXX-X'
					]
				]
			];

			// Create sections
			self::create_sections($this->msi_section);

			// Create settings
			self::create_settings($this->msi_setting);
		}
	}

	public function msinc_hook_analytics() {
		if(!is_admin()) {
			// Get Options
			global $msi_opt_name;
			$options = get_option($this->msi_opt_name);

			if($options['msi-analytics-code']) {
				if(self::isAnalytics($options['msi-analytics-code'])) {
				?>
				<!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $options['msi-analytics-code']; ?>"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());

					gtag('config', '<?php echo $options['msi-analytics-code']; ?>');
				</script>
				<?php
				} else {
					MsiCore::msinc_error('Google Analytics tracking code invalid.');
				}
			} else {
				MsiCore::msinc_error('Google Analytics tracking code missing.');
			}
		}
	}

	public function isAnalytics($str) {
		// Validate string to make sure it matches up with the analytics UA code format
	  if(preg_match('/^ua-\d{4,9}-\d{1,4}$/i', strval($str))){ return true; }
		else { return false; }
	}
}
