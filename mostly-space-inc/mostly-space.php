<?php
/**
 * @wordpress-plugin
 * Plugin Name: Mostly Space Inc.
 * Version: 1.0.0
 * Plugin URI: https://joshvance.com/
 * Description: Bring back the concept of a game that refuses to die.
 * Author: Josh Vance
 * Author URI: https://joshvance.com/
 * Requires at least: 5.3.2
 * Tested up to: 5.3.2
 * License:					 GPL-2.0+
 * License URI:			 http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package Mostly Space Inc.
 * @author Josh Vance
 * @since 1.0.0
 */

$msi_version = '1.0.0';

// Abort if this file is called directly.
if(!defined('WPINC')) { die; }

// Define plugin version
define('PLUGIN_NAME_VERSION', $msi_version);

// Include Redux Framework if not already
if(!class_exists('ReduxFramework') && file_exists(dirname(__FILE__) . '/includes/redux-framework/ReduxCore/framework.php')) {
	require_once(dirname(__FILE__) . '/includes/redux-framework/ReduxCore/framework.php');
}

// Include Metabox if not already
if(!class_exists('RW_Meta_Box') && file_exists(dirname(__FILE__) . '/includes/meta-box/meta-box.php')) {
	require_once(dirname(__FILE__) . '/includes/meta-box/meta-box.php');
}

// Includes
require_once plugin_dir_path(__FILE__) . 'classes/msi-settings.class.php';				// Settings Page
require_once plugin_dir_path(__FILE__) . 'classes/msi-core.class.php';					// Core Plugin Class
require_once plugin_dir_path(__FILE__) . 'admin/msi-admin.php';							// Admin specific functions
require_once plugin_dir_path(__FILE__) . 'public/msi-public.php';						// Public specific functions
require_once plugin_dir_path(__FILE__) . 'classes/msi-galaxy.class.php';				// Galaxy Core Functions
require_once plugin_dir_path(__FILE__) . 'classes/msi-ships.class.php';					// Ship functions

// Load for admin pages only
if(is_admin()) {
	$msi_admin = new MsiAdmin;
}

// Load for public pages only
if(!is_admin()) {
	$msi_public = new MsiPublic;
}

// Always load
$msi_settings = new MsiSettings;
$msi_core = new MsiCore;
$msi_galaxy = new MsiGalaxy;
$msi_ships = new MsiShips;

// Called on plugin activation
function activate_msinc() {
	require_once plugin_dir_path(__FILE__) . 'classes/msi-activate.php';
	$msi_activate = new MsiActivate;
}

// Called on plugin deactivation
function deactivate_msinc() {
	require_once plugin_dir_path(__FILE__) . 'classes/msi-deactivate.php';
	$msi_deactivate = new MsiDeactivate;
}

// Called on plugin uninstall
function uninstall_msinc() {
	require_once plugin_dir_path(__FILE__) . 'classes/msi-uninstall.php';
	$msi_uninstall = new MsiUninstall;
}

// Registering activation/deactivation hooks
register_activation_hook(__FILE__, 'activate_msinc');
register_deactivation_hook(__FILE__, 'deactivate_msinc');
register_uninstall_hook(__FILE__, 'uninstall_msinc');

// Called when an undefined class is created
/*function msi_autoload($class) {
	if(preg_match('/\A\w+\Z/', $class)) {
		$allowed_classes = [
			'msi-core',
			'msi-solar-systems',
			'msi-celestial-bodies',
			'msi-analytics',
			'msi-functions',
			'msi-settings'
		];
		switch($class) {
			case 'msi-core': 							// Game Core
				$class = 'MsiCore';
				break;
			case 'msi-galaxy';						// Solar Systems
				$class = 'MsiGalaxy';
				break;
			case 'msi-analytics':					// All tracking goes here
				$class = 'MsiAnalytics';
				break;
			case 'msi-functions':					// Game Functions
				$class = 'MsiFunctions';
				break;
			case 'msi-settings':					// Settings page for game
				$class = 'MsiSettings';
				break;
		}
		// Verify we aren't catching classes that aren't meant for our game.
		if(in_array($class, $allowed_classes)) {
			include 'classes/' . $class . '.class.php';
		}
	}
}
spl_autoload_register('msi_autoload');*/
