<?php
/**
 * Public facing functionality of the plugin
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/public
 */

class MsiPublic extends MsiCore {

	// Construct
	public function __construct() {
		add_action('wp_enqueue_styles', [$this, 'enqueue_public_styles']);
		add_action('wp_enqueue_scripts', [$this, 'enqueue_public_scripts']);
	}

	// Enqueue public facing styles
	public function enqueue_public_styles() {
		wp_enqueue_style('msi-public-style', plugin_dir_path(__FILE__) . 'public/css/msi-public-style.css');
	}

	// Enqueue public facing scripts
	public function enqueue_public_scripts() {
		wp_enqueue_script('msi-public-script', plugin_dir_path(__FILE__) . 'public/js/msi-public-script.js', ['jquery'], false, true); // Loads in footer
	}
}
