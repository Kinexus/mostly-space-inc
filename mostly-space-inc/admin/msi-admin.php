<?php
/**
 * Admin facing functionality of the plugin
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/admin
 */

class MsiAdmin extends MsiCore {
	public function __construct() {
		add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_scripts']);
	}

	public function enqueue_admin_scripts() {
		// Admin Styles
		wp_register_style('msi-admin-style', '/wp-content/plugins/mostly-space-inc/admin/css/msi-admin-style.css');
		wp_enqueue_style('msi-admin-style', '/wp-content/plugins/mostly-space-inc/admin/css/msi-admin-style.css');

		// Admin Scripts
		wp_register_script('msi-admin-script', '/wp-content/plugins/mostly-space-inc/admin/js/msi-admin-script.js');
		wp_enqueue_script('msi-admin-script', '/wp-content/plugins/mostly-space-inc/admin/js/msi-admin-script.js', ['jquery'], false, true); // Loads in footer
	}
}
