jQuery(document).ready(function(){
	// Big Bang
	star_canvas = document.getElementById('star-canvas');
	space = star_canvas.getContext("2d");

	system_canvas = document.getElementById('system-canvas');
	system = system_canvas.getContext("2d");

	star_canvas.width = window.innerWidth * 2;
	star_canvas.height = window.innerHeight;
	system_canvas.width = window.innerWidth * 2;
	system_canvas.height = window.innerHeight;

	makeStars();
	makeStar();

});

function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}

function makeStars() {
	var estrelas = [];

	function createStars() {
		estrelas = [];
		for (i = 0; i < star_canvas.width * 3; i++) {
			estrelas.push({
				x: Math.random() * star_canvas.width,
				y: Math.random() * star_canvas.height,
				rad: Math.random(),
			});
		}
	}

	function draw() {
		var brightness = getRandomInt(255);
		space.clearRect(0, 0, star_canvas.width, star_canvas.height);
		space.fillStyle = "rgba(255, 255, 255, " + brightness + ")";
		for (i = 0; i < estrelas.length; i++) {
		  	var e = estrelas[i];
			space.beginPath();
			space.arc(e.x, e.y, e.rad, 0, 2 * Math.PI);
			space.fill();
		}
	}

	function update() {
		for (i = 0; i < estrelas.length; i+=4) {
	  	//for (i = 0; i < estrelas.length; i++) {
	    	var e = estrelas[i];
	    	e.rad = Math.random();
		}
	}

	createStars();

	function pulse() {
		update();
		draw();
		requestAnimationFrame(pulse);
	}

	pulse();

	window.onresize = function() {
		star_canvas.width = window.innerWidth;
		star_canvas.height = window.innerHeight;
		createStars();
	}
}

function makeStar() {
	drawStar();
	function drawStar() {
		var grd = system.createRadialGradient(window.innerHeight / 2, window.innerHeight / 2, 250, window.innerHeight / 2, window.innerHeight / 2, 500);
		grd.addColorStop(0, "rgba(255, 250, 242, 1)");
		grd.addColorStop(1, "rgba(255, 225, 178, 1)");

		system.beginPath();
		system.arc(window.innerHeight / 2, window.innerHeight / 2, 500, 0, 2 * Math.PI);
		system.shadowBlur = 200;
		system.shadowColor = "rgba(255, 236, 204, 1)";
		system.fillStyle = grd;
		system.fill();
	}

	window.onresize = function() {
		system_canvas.width = window.innerWidth;
		system_canvas.height = window.innerHeight;
		drawStar();
	}
}
