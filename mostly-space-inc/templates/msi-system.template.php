<?php
/**
 * Page template for solar systems.
 *
 * @link https://joshvance.com
 * @since 1.0.0
 * @package Mostly Space Inc
 * @subpackage Mostly Space Inc/Templates/System Template
 *
 * Template Name: System Template
 */

wp_head();

// Draw the system
msinc_draw_system();

wp_footer();

function msinc_draw_system() {
	?>
		<canvas class="space" id="star-canvas"></canvas>
		<canvas class="space" id="system-canvas"></canvas>
	<?php
}
